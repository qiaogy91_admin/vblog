// 异常处理通用包

package common

import (
	"encoding/json"
)

// Exception 自定义异常
type Exception struct {
	HttpCode  *int   `json:"http_code"`
	ErrorCode *int   `json:"error_code"` // 0 表示正常，nil 表示状态未知
	Message   string `json:"message"`    // 具体的衣长信息
}

// NewException 默认沿用HTTP code
func NewException(code int, message string) *Exception {
	return &Exception{
		HttpCode:  &code,
		ErrorCode: &code,
		Message:   message,
	}
}

// Error 类型的对象，默认打印时会打印Error() 方法的返回值；其他类型的对象默认打印时会打印String() 方法的返回值
func (e *Exception) Error() string {
	b, _ := json.MarshalIndent(e, " ", "  ")
	return string(b)
}

func (e *Exception) SetExceptionHttpCode(code int) *Exception {
	e.HttpCode = &code
	return e
}
func (e *Exception) SetExceptionBodyCode(code int) *Exception {
	e.ErrorCode = &code
	return e
}
