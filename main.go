package main

import (
	"gitee.com/qiaogy91_admin/vblog/cmd"
	"github.com/spf13/cobra"
)

func main() {
	if err := cmd.Exec(); err != nil {
		cobra.CheckErr(err)
	}
}
