package protocol

import (
	"context"
	"fmt"

	"gitee.com/qiaogy91_admin/vblog/conf"
	"gitee.com/qiaogy91_admin/vblog/ioc"
	"google.golang.org/grpc"
	"net"
)

func NewGrpcServer() *GrpcServer {
	return &GrpcServer{
		server: grpc.NewServer(),
	}
}

type GrpcServer struct {
	server *grpc.Server
}

func (g *GrpcServer) Start() error {
	// 获取所有的grpc controller 并完成服务注册
	for _, v := range ioc.LoadGrpcController() {
		v.Registry(g.server)
	}

	// 启动TCP 监听
	listen, err := net.Listen("tcp", conf.GetConfig().GrpcServerAddr())
	if err != nil {
		return err
	}

	fmt.Printf("GRPC Server start at: %s\n", conf.GetConfig().GrpcServerAddr())
	return g.server.Serve(listen)
}

func (g *GrpcServer) Shutdown(ctx context.Context) {
	g.server.GracefulStop()
	fmt.Printf("gRPCServer shutdown\n")
}
