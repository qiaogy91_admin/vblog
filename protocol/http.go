package protocol

import (
	"context"
	"errors"
	"fmt"
	"gitee.com/qiaogy91_admin/vblog/conf"
	"github.com/gin-gonic/gin"
	"net/http"
	"time"
)

// NewHttpServer 协议服务器定义，所有的Web 框架都是自定义路由的一种实现
func NewHttpServer(r *gin.Engine) *HttpServer {
	// 根据配置创建HttpServer 实例对象
	return &HttpServer{
		server: &http.Server{
			Addr:         conf.GetConfig().HttpServerAddr(), // 要将方法封装到对象中，不要写在过程中；这里NewHttp 是一个过程
			Handler:      r,                                 // 使用的路由
			ReadTimeout:  5 * time.Second,                   // 读取客户端request 的超时时间
			WriteTimeout: 5 * time.Second,                   // 响应客户端response 的超时时间爱你
		},
	}
}

type HttpServer struct {
	server *http.Server
}

func (h *HttpServer) Start() error {
	fmt.Printf("HttpServer start at %s\n", conf.GetConfig().HttpServerAddr())
	err := h.server.ListenAndServe()
	if errors.Is(err, http.ErrServerClosed) {
		return nil // 调用Shutdown() 后被主动关闭了，忽略这种错误
	}
	return err // 其他错误正常抛出
}
func (h *HttpServer) Shutdown(ctx context.Context) {
	if err := h.server.Shutdown(ctx); err != nil {
		fmt.Printf("shutdown failed: %v\n", err)
	}
	fmt.Printf("HttpServer shutdown\n")
}
