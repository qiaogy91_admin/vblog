package middleware

import (
	"fmt"
	"gitee.com/qiaogy91_admin/vblog/apps/user"
	"gitee.com/qiaogy91_admin/vblog/common"
	"gitee.com/qiaogy91_admin/vblog/ioc"
	"github.com/gin-gonic/gin"
	"net/http"
)

// 并非所有请求都需要进行中间件检查

/*
业务模块如何使用：
	方式（1）：
			vim vblog/apps/blog/api/http.go  在api handler 路由初始化时添加中间件配置
			func (h *Handler) Registry(r gin.IRouter) {
				// r 是全局router，r.Use() 会将中间件应用给所有路由条目
				// 应该为每个模块单独一个子router，并为子router 按需加载中间件

				blogRouter := r.Group("/vblog/api/v1/blogs/")
				blogRouter.GET("")// 不会经过中间件的请求（查询）

				blogRouter.Use(middleware.Authentication) // 中间件, 在调用use 方法后注册的路由，中间件会生效

				blogRouter.POST("", CreateBlog)// 需要经过中间件的请求（增、删、改）
				blogRouter.POST("", UpdateBlog)
			}
			每个模块都得加入代码，比较繁琐;并且如果A模块开发者忘记生成子路由，直接使用r.use() 会影响其他模块开发者
	方式（2）：
		我希望Registry 函数接收的r参数，本身就是一个子路由；因此将子路由的生成过程，挪到Ioc中处理:
		- cmd 中，调用Ioc 执行初始化时，传递ProjectPrefix；
			ioc.InitHandler(ProjectPrefix, r)

		- Ioc 中调用模块的Init方法执行路由注册时，新生成一个subRouter 作为参数传入
			func InitHandler(projectPrefix string, r gin.IRouter) error {
				for name, h := range HandlerContainer {
					if err := h.Init(); err != nil {
						return fmt.Errorf("handler 初始化，%s 模块初始化失败\n", h.Name())
					}
					subRouter := r.Group(path.Join(projectPrefix, name))
					h.Registry(subRouter)
				}
				return nil
			}
		- 各业务模块中进行使用
			vim vblog/apps/blog/api/http.go
			func (h *Handler) Registry(r gin.IRouter) {
				r.GET("")// 不会经过中间件的请求（查询）
				r.Use(middleware.Authentication) // 中间件, 在调用use 方法后注册的路由，中间件会生效
				r.POST("", CreateBlog)// 需要经过中间件的请求（增、删、改）
				r.POST("", UpdateBlog)
			}



*/

// 所有硬编码需要定义为常量

func Authentication(ctx *gin.Context) {
	// 1. 从cookie 中获取Token
	cookie, err := ctx.Cookie("VblogToken")
	if err != nil {
		fmt.Printf("%v", err)
		ctx.AbortWithStatusJSON(http.StatusBadRequest, common.NewException(http.StatusBadRequest, "未获取到Token"))
		return
	}

	// 2. 从Ioc 获取User 模块，调用User 模块ValidateToken 进行校验
	req := user.ValidateTokenRequest{Token: cookie}

	controller := ioc.GetController(user.AppName).(user.Service)
	token, err := controller.ValidateToken(ctx, &req)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, err)
		return
	}

	// 3. 把User 对象注入ctx 上下文中，方便后续Handler 能够直接从ctx 中获取用户的身份信息
	ctx.Set("info", token) // controller 中通过 ctx.Get() 来获取，然后断言后使用，比如create blog 时，用户创建blog时不用再提供author信息了，从ctx 中获取

}
