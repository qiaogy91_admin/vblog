package cmd

import (
	// 注册所有的APP 模块
	_ "gitee.com/qiaogy91_admin/vblog/apps"
	// 导入协议服务器模块（httpServer、grpcServer），跟Ioc 没有什么关系
	"gitee.com/qiaogy91_admin/vblog/protocol"

	"context"
	"fmt"
	"gitee.com/qiaogy91_admin/vblog/conf"
	"gitee.com/qiaogy91_admin/vblog/ioc"
	"github.com/gin-gonic/gin"
	"github.com/spf13/cobra"
	"os"
	"os/signal"
	"syscall"
)

var (
	configType string
	configFile string
)

var rootCmd = &cobra.Command{
	Use:   "vblog-api",
	Short: "vblog backend",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		// 命令的执行逻辑；root command 中的run 一般不会定义逻辑，逻辑都拆分到其他子command 中执行
		err := cmd.Help()
		cobra.CheckErr(err)
	},
}

var startCmd = &cobra.Command{
	Use:   "start",
	Short: "start server",
	Run: func(cmd *cobra.Command, args []string) {
		// 配置初始化
		switch configType {
		case "file":
			cobra.CheckErr(conf.InitializeFromToml(configFile))
		default:
			cobra.CheckErr(conf.InitializeFromEnv())
		}

		// Controller 初始化
		err := ioc.InitController()
		cobra.CheckErr(err)

		// Handler 初始化
		r := gin.Default()
		err = ioc.InitHandler("/vblog/api/v1", r)
		cobra.CheckErr(err)

		// Protocol HttpServ 启动
		httpServer := protocol.NewHttpServer(r)
		go func() {
			cobra.CheckErr(httpServer.Start())
		}()

		// Protocol grpcServ 启动
		grpcServer := protocol.NewGrpcServer()
		go func() {
			cobra.CheckErr(grpcServer.Start()) // start 内部会先执行grpc 服务的注册，而后再启动TCP监听
		}()

		// 主协程监听OS信号
		ch := make(chan os.Signal)
		signal.Notify(ch, syscall.SIGTERM, syscall.SIGQUIT, syscall.SIGINT, syscall.SIGHUP)
		s := <-ch // 阻塞，从channel 中获取信号
		fmt.Printf("主线程收到信号： %v  exit.....\n", s)

		// 收到信号后执行清理操作
		httpServer.Shutdown(context.Background()) // 关闭 httpServ
		grpcServer.Shutdown(context.Background()) // 关闭 grpcServ
		conf.GetConfig().StopDB()                 // 关闭 Gorm 连接池
	},
}

func init() {
	startCmd.Flags().StringVarP(&configType, "config-type", "t", "file", "use which way to load configuration [file|env]， default is file")
	startCmd.Flags().StringVarP(&configFile, "config-file", "f", "conf.toml", "path to configuration file. default is conf.toml")

	rootCmd.AddCommand(startCmd)
}

func Exec() error {
	return rootCmd.Execute()
}
