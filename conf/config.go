package conf

import (
	"encoding/json"
	"fmt"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"sync"
	"time"
)

// Config 当前程序的配置对象：程序所需的所有配置，都从这个对象身上去获取

type Config struct {
	MySql *MySql `json:"mysql" toml:"mysql"` // toml 标签表示从配置的 [mysql] 读取数据进行映射
	Http  *Http  `json:"http" toml:"http"`
	Grpc  *Grpc  `json:"grpc" toml:"grpc"`
}

type Http struct {
	Addr string `json:"addr" toml:"addr" env:"HTTP_ADDR"`
	Port int    `json:"port" toml:"port" env:"HTTP_PORT"`
}

type Grpc struct {
	Addr string `json:"addr" toml:"addr" env:"GRPC_ADDR"`
	Port int    `json:"port" toml:"port" env:"GRPC_PORT"`
}

type MySql struct {
	Host     string `json:"host" toml:"host" env:"MYSQL_HOST"`
	Port     int    `json:"port" toml:"port" env:"MYSQL_PORT"`
	DataBase string `json:"database" toml:"database" env:"MYSQL_DATABASE"`
	Username string `json:"username" toml:"username" env:"MYSQL_USERNAME"`
	Password string `json:"password" toml:"password" env:"MYSQL_PASSWORD"`

	lock sync.Mutex
	db   *gorm.DB
}

func (c *Config) String() string {
	b, _ := json.MarshalIndent(c, " ", "  ")
	return string(b)
}

// SetORM 获取全局db 对象 通常将mysql 的连接池等公共配置，也放在这个对象上
// 利用已有的字段信息，来初始化MySQL 链接池
func (c *Config) SetORM() {
	// 整个函数体加锁
	c.MySql.lock.Lock()
	defer c.MySql.lock.Unlock()

	// 单例模式
	if c.MySql.db != nil {
		return
	}

	// 1.获取DB对象：
	var err error
	dns := fmt.Sprintf(
		"%s:%s@tcp(%s:%d)/%s?charset=utf8mb4&parseTime=true&loc=Local",
		c.MySql.Username, c.MySql.Password, c.MySql.Host, c.MySql.Port, c.MySql.DataBase,
	)

	c.MySql.db, err = gorm.Open(mysql.Open(dns), &gorm.Config{}) // 链接数据库
	if err != nil {                                              // 有问题直接中断，后续代码没必要执行
		panic("链接数据库实例失败::" + err.Error())
	}

	// 2.设置连接池
	sqlDB, _ := c.MySql.db.DB()         // Gorm 将连接池对象设置为了DB对象的一个属性
	sqlDB.SetMaxOpenConns(100)          // 池子内最大的链接数量
	sqlDB.SetMaxIdleConns(10)           // 最大的空闲链接，即空闲状态的链接最大保留10个，其他空闲链接将被关闭
	sqlDB.SetConnMaxLifetime(time.Hour) // 执行完一次SQL毕后不要关闭链接，保持1h；以便在同一连接上执行多个查询和事务
}

func (c *Config) GetORM() *gorm.DB {
	if c.MySql.db == nil {
		panic("请先调用SetORM 初始化连接池")
	}
	return c.MySql.db
}

func (c *Config) HttpServerAddr() string {
	return fmt.Sprintf("%s:%d", c.Http.Addr, c.Http.Port)
}

func (c *Config) GrpcServerAddr() string {
	return fmt.Sprintf("%s:%d", c.Grpc.Addr, c.Grpc.Port)
}

func (c *Config) StopDB() {
	if c.MySql.db == nil {
		return
	}
	sqlDB, _ := c.MySql.db.DB()
	if err := sqlDB.Close(); err != nil {
		fmt.Printf("stop db pool failed: %v\n", err)
	}
	fmt.Printf("Gorm Pool shutdown\n")
}
