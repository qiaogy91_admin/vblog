package conf_test

import (
	"gitee.com/qiaogy91_admin/vblog/conf"
	"testing"
)

func TestTomlDecodeFile(t *testing.T) {
	conf.InitializeFromToml("test/config.toml")
	t.Log(conf.GetConfig()) // 解析后的全局对象

}

func TestLoadConfigFromEnv(t *testing.T) {
	conf.InitializeFromEnv()
	t.Log(conf.GetConfig())
}
