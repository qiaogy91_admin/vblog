package conf

import (
	"fmt"
	"gitee.com/qiaogy91_admin/vblog/common"
	"github.com/BurntSushi/toml"
	"github.com/caarlos0/env/v6"
	"sync"
)

// config 实例化一个配置对象
var config = &Config{
	MySql: &MySql{
		Host:     "",
		Port:     0,
		DataBase: "",
		Username: "",
		Password: "",
		lock:     sync.Mutex{},
		db:       nil,
	},
	Http: &Http{
		Addr: "",
		Port: 0,
	},
	Grpc: &Grpc{
		Addr: "",
		Port: 0,
	},
}

// InitializeFromToml 为config结构体赋值，从toml 配置文件读取配置数据后，即给Config 结构体对象填充字段信息
func InitializeFromToml(path string) error {
	if _, err := toml.DecodeFile(path, config); err != nil {
		return common.NewException(-1, fmt.Sprintf("配置文件加载失败:%s", err.Error()))
	}
	config.SetORM()
	return nil
}

// InitializeFromEnv 为config结构体赋值，从env 环境变量读取
func InitializeFromEnv() error {
	if err := env.Parse(config); err != nil {
		return common.NewException(-1, fmt.Sprintf("环境变量加载失败:%s", err.Error()))
	}
	config.SetORM()
	return nil
}

// GetConfig 返回实例化后的config 对象
func GetConfig() *Config {
	if config.MySql.Host == "" || config.MySql.db == nil {
		panic("请调用相关Load 方法加载配置文件后，再运行程序；LoadConfigFromToml、LoadConfigFromEnv")

	}
	return config
}
