package ioc

import "C"
import (
	"fmt"
	"google.golang.org/grpc"
)

// ControllerObject 模块对象的约束
type ControllerObject interface {
	Init() error  // impl 对象的初始化方法，用来解决依赖
	Name() string // impl 对象的名称
}

// IOC 容器，存储所有的模块对象。可对所有模块对象进行统一管理，比如初始化
var container = map[string]ControllerObject{}

// RegistryController 将依赖注入容器
func RegistryController(obj ControllerObject) {
	container[obj.Name()] = obj
}

// GetController 从容器获取依赖
func GetController(name string) ControllerObject {
	obj, ok := container[name]
	if !ok {
		panic("没有找到该模块")
	}
	return obj
}

// InitController 对容器中所有的impl 对象进行初始化，即执行对象的 Init() 方法
func InitController() error {
	for _, c := range container {
		if err := c.Init(); err != nil {
			return fmt.Errorf("Controller 初始化，%s 初始化异常:%s\n", c.Name(), err.Error())
		}
		fmt.Printf("[controller] %s loaded\n", c.Name())
	}
	return nil
}

// grpcControllerObject 注册进容器的对象会有普通impl 的实现、以及grpc 的impl 实现；
type grpcControllerObject interface {
	Registry(s *grpc.Server)
}

// LoadGrpcController 取出所有grpc controller 对象
func LoadGrpcController() (arr []grpcControllerObject) {
	for _, c := range container {
		v, ok := c.(grpcControllerObject) // 通过断言，看其是否实现了服务注册的功能
		if !ok {
			continue
		}
		arr = append(arr, v)
	}
	return
}
