package ioc

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"path"
)

type HandlerObject interface {
	Registry(gin.IRouter)
	Init() error
	Name() string
}

var HandlerContainer = map[string]HandlerObject{}

func RegistryHandler(obj HandlerObject) {
	HandlerContainer[obj.Name()] = obj
}

func GetHandler(name string) HandlerObject {
	obj, ok := HandlerContainer[name]
	if !ok {
		panic("没有找到该模块")
	}

	return obj
}

// InitHandler 初始化所有的Handler 模块
func InitHandler(projectPrefix string, r gin.IRouter) error {
	for name, h := range HandlerContainer {
		// 1. 调用handler 的 Init() 方法，来解决依赖
		if err := h.Init(); err != nil {
			return fmt.Errorf("handler 初始化，%s 模块初始化失败\n", h.Name())
		}
		// 2. 调用handler 的Registry 方法，来进行路由注册（子路由的命名=项目前缀+模块名称；即  "/vblog/api/v1/" + "blogs"）
		subRouter := r.Group(path.Join(projectPrefix, name))
		h.Registry(subRouter)
	}
	return nil
}
