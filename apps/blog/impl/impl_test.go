package impl_test

import (
	"context"
	"fmt"
	"testing"
	"gitee.com/qiaogy91_admin/vblog/apps/blog"
	"gitee.com/qiaogy91_admin/vblog/ioc"
	"gitee.com/qiaogy91_admin/vblog/test"
)

var controller blog.Service
var ctx = context.Background()

func init() {
	test.DevelopmentSetup()
	controller = ioc.GetController(blog.AppName).(blog.Service)
}

// func (i *impl) QueryBlog(ctx context.Context, ins *blog.QueryBlogRequest) (*blog.BlogsSet, error)
func TestQueryBlog(t *testing.T) {
	ins := &blog.QueryBlogRequest{
		PageNum:  1,
		PageSize: 20,
		KeyWord:  "tes",
		Author:   "",
	}

	res, err := controller.QueryBlog(ctx, ins)
	if err != nil {
		t.Fatalf("%v", err)
	}
	fmt.Println(res)
}

func TestDescribeBlog(t *testing.T) {
	ins := &blog.DescribeBlogRequest{
		ID: 1,
	}
	res, err := controller.DescribeBlog(ctx, ins)
	if err != nil {
		t.Fatalf("%+v", err)
	}
	t.Logf("%+v", res)
}

func TestUpdateBlog(t *testing.T) {
	ins := &blog.UpdateBlogRequest{
		ID: 1,
		CreateBlogRequest: blog.CreateBlogRequest{
			Title:   "修改01",
			Author:  "admin03",
			Content: "xxx",
			Tags:    nil,
			Status:  0,
		},
	}

	res, err := controller.UpdateBlog(ctx, ins)
	if err != nil {
		t.Fatalf("%+v", err)
	}
	t.Logf("%+v", res)
}

func TestDeleteBlog(t *testing.T) {
	ins := &blog.DeleteBlogRequest{
		ID: 2,
	}

	res, err := controller.DeleteBlog(ctx, ins)
	if err != nil {
		t.Fatalf("%+v", err)
	}
	t.Logf("%+v", res)
}
