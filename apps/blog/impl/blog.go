package impl

import (
	"context"
	"gitee.com/qiaogy91_admin/vblog/apps/blog"
	"gitee.com/qiaogy91_admin/vblog/common"
	"github.com/go-playground/validator/v10"
	"net/http"
)

func (i *impl) CreateBlog(ctx context.Context, ins *blog.CreateBlogRequest) (*blog.Blog, error) {
	// 字段校验
	if err := validator.New().Struct(ins); err != nil {
		return nil, common.NewException(http.StatusBadRequest, err.Error())
	}

	// 使用构造函数，创建实例
	obj := blog.NewBlog(ins) // 因为这里将Blog 进行了拆分，不是Blog 整个结构体，不能调用  db.Save(blog_obj)
	if err := i.db.WithContext(ctx).Create(obj).Error; err != nil {
		return nil, common.NewException(http.StatusInternalServerError, err.Error())
	}

	return obj, nil
}
func (i *impl) DeleteBlog(ctx context.Context, ins *blog.DeleteBlogRequest) (*blog.Blog, error) {
	// 字段校验
	if err := validator.New().Struct(ins); err != nil {
		return nil, common.NewException(http.StatusBadRequest, err.Error())
	}

	// 先看在不在
	req := blog.NewDescribeBlogRequest()
	req.ID = ins.ID
	obj, err := i.DescribeBlog(ctx, req)
	if err != nil {
		return nil, common.NewException(http.StatusBadRequest, err.Error())
	}

	// 在数据，则进行删除
	if err := i.db.WithContext(ctx).Where("id = ?", ins.ID).Delete(blog.Blog{}).Error; err != nil {
		return nil, common.NewException(http.StatusBadRequest, err.Error())
	}
	// 操作数据库
	return obj, nil
}
func (i *impl) UpdateBlog(ctx context.Context, ins *blog.UpdateBlogRequest) (*blog.Blog, error) {
	// 字段校验
	if err := validator.New().Struct(ins); err != nil {
		return nil, common.NewException(http.StatusBadRequest, err.Error())
	}

	// 更新实例
	if err := i.db.WithContext(ctx).Model(blog.Blog{}).Where("id = ?", ins.ID).Updates(ins).Error; err != nil {
		return nil, common.NewException(http.StatusBadRequest, err.Error())
	}

	// 查询实例并返回
	req := blog.DescribeBlogRequest{ID: ins.ID}
	obj, err := i.DescribeBlog(ctx, &req)
	if err != nil {
		return nil, common.NewException(http.StatusBadRequest, err.Error())
	}
	return obj, nil
}

// QueryBlog 根据DescribeBlog中注释的原因
// 如果要返回content 内容，应该在后端将content 进行截取部分字串，而非将数据都查询出来发送给前端后，让前端进行截取
// 因为如果将大量数据（而非部分索引数据）查询出来发送给前端，这个发送过程本身会占据很大的带宽
func (i *impl) QueryBlog(ctx context.Context, ins *blog.QueryBlogRequest) (*blog.BlogsSet, error) {
	var blogSets blog.BlogsSet
	sql := i.db.WithContext(ctx)

	// 关键字参数，查询文章内容
	// 在 GORM 中使用 LIKE 条件进行模糊匹配，需要将通配符 % 直接添加到查询字符串中，而不是将它作为query 的一部分
	if ins.KeyWord != "" {
		sql = sql.Where("content like ?", "%"+ins.KeyWord+"%")
	}
	// 作者参数，查询作者名称
	if ins.Author != "" {
		sql = sql.Where("author = ?", ins.Author)
	}
	// 分页：必传参数，否则服务端直接查爆了
	offSet := (ins.PageNum - 1) * ins.PageSize // 获取第3页数据，那么就需要跳过 2*10 个数据

	// 执行查询
	err := sql.Offset(offSet).Limit(ins.PageSize).Find(&blogSets.Items).Error // 注意是将查询结果放入 blogSets 的Items 属性中
	blogSets.Total = len(blogSets.Items)
	if err != nil {
		return nil, err
	}
	return &blogSets, nil
}

// DescribeBlog 与 QueryBlog 都可以传递ID进行单条数据查询，主要区别如下：
// 1. DescribeBlog 如果找不到会返回错误、QueryBlog找不到只会返回空
// 2. QueryBlog 一般只是拉取多个数据条目的索引，这个数据不会很大（不适合包含content 数据），DescribeBlog需要包含明细
func (i *impl) DescribeBlog(ctx context.Context, ins *blog.DescribeBlogRequest) (*blog.Blog, error) {
	var obj = blog.Blog{}
	if err := i.db.WithContext(ctx).Where("id = ?", ins.ID).First(&obj).Error; err != nil {
		return nil, err
	}
	return &obj, nil
}
