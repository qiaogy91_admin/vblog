package impl

import (
	"gitee.com/qiaogy91_admin/vblog/apps/blog"
	"gitee.com/qiaogy91_admin/vblog/conf"
	"gitee.com/qiaogy91_admin/vblog/ioc"
	"gorm.io/gorm"
)

type impl struct {
	db *gorm.DB
}

var _ blog.Service = &impl{}

func (i *impl) Name() string {
	return blog.AppName
}

func (i *impl) Init() error {
	i.db = conf.GetConfig().GetORM().Debug() // 为当前controller 添加数据库连接池
	return nil
}

func init() {
	ioc.RegistryController(&impl{})
}
