// Blog 模块对外暴露的领域接口

package blog

import "context"

type Service interface {
	CreateBlog(ctx context.Context, ins *CreateBlogRequest) (*Blog, error)
	DeleteBlog(ctx context.Context, ins *DeleteBlogRequest) (*Blog, error)
	UpdateBlog(ctx context.Context, ins *UpdateBlogRequest) (*Blog, error)
	QueryBlog(ctx context.Context, ins *QueryBlogRequest) (*BlogsSet, error)
	DescribeBlog(ctx context.Context, ins *DescribeBlogRequest) (*Blog, error)
}

// QueryBlogRequest 列表查询参数
type QueryBlogRequest struct {
	PageNum  int    `form:"page_num"`  // - 分页：默认给第一页，每页20个
	PageSize int    `form:"page_size"` // 页大小
	KeyWord  string `form:"key_word"`  // - 关键字：也是模糊查询
	Author   string `form:"author"`    // - 条件过滤：根据作者过滤
}
type UpdateBlogRequest struct {
	ID int `uri:"id" binding:"required"`
	CreateBlogRequest
}
type DescribeBlogRequest struct {
	ID int `uri:"id"`
}
type DeleteBlogRequest struct {
	ID int `uri:"id"`
}

// NewQueryBlogRequest 每个方法都应该写一个构造函数来初始化 Request 对象，写业务逻辑不要偷这个懒
func NewQueryBlogRequest() *QueryBlogRequest {
	return &QueryBlogRequest{
		PageNum:  1,
		PageSize: 20,
		KeyWord:  "",
		Author:   "",
	}
}

func NewDescribeBlogRequest() *DescribeBlogRequest {
	return &DescribeBlogRequest{}
}

func NewUpdateBlogRequest() *UpdateBlogRequest {
	return &UpdateBlogRequest{}
}
