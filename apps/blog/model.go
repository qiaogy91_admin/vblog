// Blog 模块锁管理的数据

package blog

import (
	"encoding/json"
	"time"
)

// Blog 通常一个对象的模型，由Meta元数据、以及创建一个对象所需的完整数据组成（CreateBlogRequest）
// 所以此处将 CreateBlogRequest 的定义放在model 中，而不是interface.go 中
type Blog struct {
	Meta
	CreateBlogRequest
}

type BlogsSet struct {
	Total int     `json:"total"`
	Items []*Blog `json:"items"`
}

// Meta 元数据，这些数据在创建博客时，无需用户传递
type Meta struct {
	// 尽量让json 和数据库中字段名一致；因为如果不通过 `gorm:"column:xxx"` 来定义数据库字段名的话，默认会与json 标签保持一致
	ID        int               `json:"id"`
	CreateAt  int64             `json:"create_at"`
	UpdateAt  int64             `json:"update_at"`
	PublishAt int64             `json:"publish_at"`
	Tags      map[string]string `json:"tags" gorm:"serializer:json"`
}

// CreateBlogRequest 用户数据，这些数据在创建博客时，由用户传递
// 实例化出的对象，都属于用户数据对象；将作为参数传递给业务逻辑控制器对象
type CreateBlogRequest struct {
	Title   string `json:"title" validate:"required" binding:"required"`
	Author  string `json:"author" validate:"required" binding:"-"`
	Content string `json:"content" validate:"required" binding:"required"`
	// map 类型的数据Gorm 是不知道如何存储的。 gorm:"serializer:json" 标签表明，存储该字段时，直接存储他的json格式；读取该字段时，读取出来后json 反序列化后作为字段值
	Tags   map[string]string `json:"tags" gorm:"serializer:json"`
	Status STATUS            `json:"status"`
}

func (s *BlogsSet) String() string {
	bs, _ := json.MarshalIndent(s, "", "  ")
	return string(bs)

}

// NewMeta 为对象定义构造函数，是为了保证兼容性。当结构体扩冲的时候，能够只在构造函数中进行修改
func NewMeta() *Meta {
	return &Meta{CreateAt: time.Now().Unix()}
}

func NewBlog(req *CreateBlogRequest) *Blog {
	return &Blog{
		Meta:              *NewMeta(),
		CreateBlogRequest: *req,
	}
}

func NewCreateBlogRequest() *CreateBlogRequest {
	// 结构体内有一些类型在实例化时，是必须要给参数的，但这些参数用户在提交请求的时候可以不给；
	// 因此可以弄个构造函数，将其作为默认参数提供
	return &CreateBlogRequest{
		Tags:   map[string]string{},
		Status: DRAFT,
	}
}

func (b Blog) TableName() string {
	return "blog"
}
