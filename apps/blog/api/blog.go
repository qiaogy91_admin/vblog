package api

import (
	"fmt"
	"gitee.com/qiaogy91_admin/vblog/apps/blog"
	"gitee.com/qiaogy91_admin/vblog/apps/user"
	"gitee.com/qiaogy91_admin/vblog/common"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
	"time"
)

func (h *Handler) CreateBlog(c *gin.Context) {
	// 获取用户数据
	info, ok := c.Get("info")
	u := info.(*user.Token)
	if !ok {
		c.JSON(http.StatusInternalServerError, common.NewException(http.StatusInternalServerError, "未获取到token信息"))
		return
	}

	// 数据绑定：向结构体填充数据
	ins := blog.NewCreateBlogRequest()
	if err := c.ShouldBind(ins); err != nil {
		c.JSON(http.StatusInternalServerError, common.NewException(http.StatusInternalServerError, err.Error()))
		return // 绑定失败时，直接返回，不再执行后续代码
	}

	ins.Author = u.Username

	// 业务逻辑处理：调用impl 具体的业务实现
	obj, err := h.svc.CreateBlog(c, ins)
	if err != nil {
		c.JSON(http.StatusInternalServerError, common.NewException(http.StatusInternalServerError, err.Error()))
		return
	}

	// 返回用户响应
	c.JSON(200, obj)
}

func (h *Handler) QueryBlog(c *gin.Context) {
	ins := blog.NewQueryBlogRequest()
	if err := c.ShouldBindQuery(ins); err != nil {
		c.JSON(http.StatusInternalServerError, common.NewException(http.StatusInternalServerError, err.Error()))
		return
	}
	fmt.Printf("数据绑定：%+v", ins)

	obj, err := h.svc.QueryBlog(c, ins)
	if err != nil {
		c.JSON(http.StatusInternalServerError, common.NewException(http.StatusInternalServerError, err.Error()))
		return
	}
	c.JSON(200, obj)
}

func (h *Handler) DescribeBlog(c *gin.Context) {
	ins := blog.NewDescribeBlogRequest()

	if err := c.ShouldBindUri(ins); err != nil {
		c.JSON(http.StatusInternalServerError, common.NewException(http.StatusInternalServerError, err.Error()))
		return
	}
	fmt.Printf("数据绑定：%+v", ins)

	obj, err := h.svc.DescribeBlog(c, ins)
	if err != nil {
		c.JSON(http.StatusInternalServerError, common.NewException(http.StatusInternalServerError, err.Error()))
		return
	}
	c.JSON(200, obj)
}

func (h *Handler) UpdateBlog(c *gin.Context) {
	//（1）数据绑定处理
	// - params 数据绑定
	ins := blog.NewUpdateBlogRequest()
	ins.ID, _ = strconv.Atoi(c.Param("id"))

	// - Body 数据绑定
	if err := c.ShouldBind(ins); err != nil {
		c.JSON(http.StatusInternalServerError, common.NewException(http.StatusInternalServerError, err.Error()))
		return
	}

	// - author数据绑定
	info, ok := c.Get("info")
	u := info.(*user.Token)
	if !ok {
		c.JSON(http.StatusInternalServerError, common.NewException(http.StatusInternalServerError, "未获取到token信息"))
		return
	}
	ins.Author = u.Username

	//（3）调用业务实现
	obj, err := h.svc.UpdateBlog(c, ins)
	if err != nil {
		c.JSON(http.StatusInternalServerError, common.NewException(http.StatusInternalServerError, err.Error()))
	}

	// 返回响应
	c.JSON(200, obj)
}

func (h *Handler) DeleteBlog(c *gin.Context) {
	// 模拟网络阻塞
	time.Sleep(2 * time.Second)

	ins := &blog.DeleteBlogRequest{}
	if err := c.ShouldBindUri(ins); err != nil {
		c.JSON(http.StatusInternalServerError, common.NewException(http.StatusInternalServerError, err.Error()))
		return
	}

	obj, err := h.svc.DeleteBlog(c, ins)
	if err != nil {
		c.JSON(http.StatusInternalServerError, common.NewException(http.StatusInternalServerError, err.Error()))
		return
	}

	c.JSON(200, obj)
}
