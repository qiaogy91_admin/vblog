// 可以作为接口给外部使用的有多种途径，比如HTTP、Grpc、Thrift、TCP 等。此处只是HTTP 接口，因此将当前文件命名为http.go

package api

import (
	"gitee.com/qiaogy91_admin/vblog/apps/blog"
	"gitee.com/qiaogy91_admin/vblog/ioc"
	"gitee.com/qiaogy91_admin/vblog/protocol/middleware"
	"github.com/gin-gonic/gin"
)

type Handler struct {
	svc blog.Service // 该对象内部封装一个实现了 blog.Service 接口约束的svc 对象，通过svc 的领域方法来对领域内数据进行操作
}

func (h *Handler) Init() error {
	// 依赖处理：从Ioc 容器中获取所需的依赖，获取出的是BaseObject 类型对象，可放心断言为 blog.Service 类型
	h.svc = ioc.GetController(blog.AppName).(blog.Service)
	return nil
}

// Registry 路由初始化：路由命名最佳实践  /服务名/api/版本/资源 ，比如 GET  /vblog/api/v1/blogs
func (h *Handler) Registry(subRouter gin.IRouter) {
	// 不会经过中间件的请求（查询）
	subRouter.GET("", h.QueryBlog) // 在调用use 方法后注册的路由，中间件会生效

	// 需要经过中间件的请求（增、删、改）
	subRouter.Use(middleware.Authentication)
	subRouter.POST("", h.CreateBlog)
	subRouter.GET(":id", h.DescribeBlog)
	subRouter.PUT(":id", h.UpdateBlog)
	subRouter.DELETE(":id", h.DeleteBlog)
}

func (h *Handler) Name() string {
	// 注意这里不会重名，因为controller、handler 模块注册进入的是不同的Ioc 容器
	return blog.AppName
}

func init() {
	ioc.RegistryHandler(&Handler{})
}
