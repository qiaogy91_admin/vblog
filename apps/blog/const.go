package blog

import (
	"fmt"
)

type STATUS int

// AppName 业务单元的名称，使用常量来定义
const (
	AppName        = "blog"
	DRAFT   STATUS = iota
	PUBLISHED
)

func (s *STATUS) MarshalJSON() ([]byte, error) {
	switch *s {
	case DRAFT:
		return []byte(`"草稿"`), nil
	case PUBLISHED:
		return []byte(`"已发布"`), nil
	}
	return []byte(fmt.Sprintf("%d", *s)), nil
}

func (s *STATUS) UnmarshalJSON(b []byte) error {

	switch string(b) {
	case "草稿":
		*s = DRAFT
		return nil

	case "已发布":
		*s = PUBLISHED
		return nil
	}
	return nil
}
