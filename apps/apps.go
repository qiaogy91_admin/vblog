package apps

import (
	// 注册controller Impl 对象进Ioc
	_ "gitee.com/qiaogy91_admin/vblog/apps/blog/impl"    // blog 模块
	_ "gitee.com/qiaogy91_admin/vblog/apps/comment/impl" // comment 模块
	_ "gitee.com/qiaogy91_admin/vblog/apps/user/impl"    // user 模块

	// 注册handler 对象进Ioc
	_ "gitee.com/qiaogy91_admin/vblog/apps/blog/api" // blog 模块
	_ "gitee.com/qiaogy91_admin/vblog/apps/user/api" // user 模块
)
