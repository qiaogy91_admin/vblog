package impl

import (
	"context"
	"gitee.com/qiaogy91_admin/vblog/apps/comment"
	"github.com/go-playground/validator/v10"
)

func (i *impl) CreateComment(ctx context.Context, req *comment.CreateCommentRequest) (*comment.Comment, error) {
	ins := &comment.Comment{
		Meta: &comment.Meta{
			CreateAt: 0,
			UpdateAt: 0,
			DeleteAt: 0,
		},
		Spec: &comment.CreateCommentRequest{
			UserId:  1,
			BlogId:  100,
			Content: "xxx",
		},
	}

	if err := validator.New().Struct(req); err != nil {
		return nil, err
	}

	return ins, nil
}
