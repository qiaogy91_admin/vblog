package impl

import (
	"gitee.com/qiaogy91_admin/vblog/apps/comment"
	"gitee.com/qiaogy91_admin/vblog/conf"
	"gitee.com/qiaogy91_admin/vblog/ioc"
	"google.golang.org/grpc"
	"gorm.io/gorm"
)

type impl struct {
	comment.UnimplementedRpcServer
	db *gorm.DB
}

// Init 添加数据库连接池
func (i *impl) Init() error {
	i.db = conf.GetConfig().GetORM().Debug()
	return nil
}

func (i *impl) Name() string {
	return comment.AppName
}

// Registry 比普通impl 的实现要多一个方法，来将impl 对象，注册到grpc server 中
func (i *impl) Registry(s *grpc.Server) {
	comment.RegisterRpcServer(s, i) // RegisterBlogRPCServer(grpcServer, &impl.Impl{})
}

func init() {
	ioc.RegistryController(&impl{})
}
