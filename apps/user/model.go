package user

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"github.com/rs/xid"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
	"time"
)

const ExpireTime = time.Hour * 24

//const ExpireTime = time.Second * 60

type User struct {
	*gorm.Model
	*CreateUserRequest
}

func (u *User) TableName() string {
	return "users"
}
func (u *User) String() string {
	bs, _ := json.MarshalIndent(u, "", "  ")
	return string(bs)
}
func (u *User) BuildPassword() error {
	bs, err := bcrypt.GenerateFromPassword([]byte(u.Password), 10)
	if err != nil {
		return err
	}
	u.Password = base64.StdEncoding.EncodeToString(bs)
	return nil
}
func (u *User) CheckPassword(password string) error {

	bs, _ := base64.StdEncoding.DecodeString(u.Password)

	if err := bcrypt.CompareHashAndPassword(bs, []byte(password)); err != nil {
		return errors.New("密码错误")
	}
	return nil
}

type Token struct {
	*gorm.Model
	Username string `json:"username" gorm:"unique"`
	Token    string `json:"token"`
}

func (t *Token) String() string {
	bs, _ := json.MarshalIndent(t, "", "  ")
	return string(bs)
}
func (t *Token) TableName() string { return "token" }
func (t *Token) IsExpire() bool {
	exprAt := t.UpdatedAt.Add(ExpireTime)
	return time.Now().After(exprAt)
}

func NewToken(username string) *Token {
	return &Token{
		Model:    nil,
		Username: username,
		Token:    xid.New().String(),
	}
}
