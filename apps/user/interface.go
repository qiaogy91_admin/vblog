package user

import "context"

const AppName = "users"

type DescribeType int

const (
	DescribeById DescribeType = iota
	DescribeByName
)

type Service interface {
	CreateUser(ctx context.Context, req *CreateUserRequest) (*User, error)
	DescribeUser(ctx context.Context, req *DescribeUserRequest) (*User, error)
	Login(ctx context.Context, req *LoginRequest) (*Token, error)
	ValidateToken(ctx context.Context, req *ValidateTokenRequest) (*Token, error)
}

type CreateUserRequest struct {
	Username string `json:"username" validate:"required"`
	Password string `json:"password" validate:"required"`
}
type DescribeUserRequest struct {
	Type  DescribeType `json:"describe_type"`
	Value string       `json:"value" validate:"required"`
}

type LoginRequest struct {
	Username string `json:"username" validate:"required"`
	Password string `json:"password" validate:"required"`
}
type ValidateTokenRequest struct {
	Token string `json:"token" validate:"required"`
}
