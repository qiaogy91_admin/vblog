package api

import (
	"gitee.com/qiaogy91_admin/vblog/apps/user"
	"gitee.com/qiaogy91_admin/vblog/ioc"
	"github.com/gin-gonic/gin"
)

type Handler struct {
	svc user.Service
}

func (h *Handler) Init() error {
	h.svc = ioc.GetController(user.AppName).(user.Service)
	return nil
}

func (h *Handler) Name() string {
	return "user"
}

func (h *Handler) Registry(r gin.IRouter) {
	r.POST("", h.CreateUser)
	r.POST("/login", h.Login)
}

func init() {
	ioc.RegistryHandler(&Handler{})
}
