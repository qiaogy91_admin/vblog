package api

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"gitee.com/qiaogy91_admin/vblog/apps/user"
)

func (h *Handler) CreateUser(ctx *gin.Context) {
	req := user.CreateUserRequest{}
	if err := ctx.ShouldBind(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, err)
		return
	}

	ins, err := h.svc.CreateUser(ctx, &req)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, err)
		return
	}
	ctx.JSON(http.StatusOK, ins)

}

func (h *Handler) Login(ctx *gin.Context) {
	req := user.LoginRequest{}
	if err := ctx.ShouldBind(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, err)
		return
	}

	token, err := h.svc.Login(ctx, &req)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, err)
		return
	}

	ctx.SetCookie("VblogToken", token.Token, 3600, "/", "", false, true)
	ctx.JSON(200, token)
}
