package impl_test

import (
	"context"
	"fmt"
	"gitee.com/qiaogy91_admin/vblog/apps/user"
	"gitee.com/qiaogy91_admin/vblog/apps/user/impl"
	"gitee.com/qiaogy91_admin/vblog/conf"
	"testing"
)

var imp = &impl.Impl{}
var controller user.Service

func init() {
	if err := conf.InitializeFromEnv(); err != nil {
		fmt.Printf("init from env failed: %v\n", err)
	}
	fmt.Printf("%v", conf.GetConfig())

	_ = imp.Init()
	controller = imp
}

func TestCreateUser(t *testing.T) {
	req := user.CreateUserRequest{
		Username: "admin01",
		Password: "redhat",
	}
	ins, err := controller.CreateUser(context.Background(), &req)
	if err != nil {
		t.Fatalf("%s", err.Error())
	}
	t.Logf("%+v", ins)
}
