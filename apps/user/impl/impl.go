package impl

import (
	"gitee.com/qiaogy91_admin/vblog/apps/user"
	"gitee.com/qiaogy91_admin/vblog/conf"
	"gitee.com/qiaogy91_admin/vblog/ioc"
	"gorm.io/gorm"
)

type Impl struct {
	db *gorm.DB
}

func (i *Impl) Init() error {
	i.db = conf.GetConfig().GetORM().Debug()
	return nil
}
func (i *Impl) Name() string {
	return user.AppName
}
func (i *Impl) CreateTable() error {
	if err := i.db.AutoMigrate(&user.User{}, &user.Token{}); err != nil {
		return err
	}
	return nil
}

func init() {
	var controller = &Impl{}
	ioc.RegistryController(controller)
}
