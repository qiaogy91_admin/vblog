package impl

import (
	"context"
	"gitee.com/qiaogy91_admin/vblog/apps/user"
	"gitee.com/qiaogy91_admin/vblog/common"
	"github.com/go-playground/validator/v10"
	"gorm.io/gorm/clause"
	"net/http"
	"time"
)

// ExpireTime Token过期时间

// 先写这个，IDE会报错，然后自动进行patch，爽歪歪
var _ user.Service = &Impl{}

func (i *Impl) CreateUser(ctx context.Context, req *user.CreateUserRequest) (*user.User, error) {
	// 数据校验
	if err := validator.New().Struct(req); err != nil {
		return nil, common.NewException(http.StatusBadRequest, err.Error())
	}
	// 用户模型
	ins := user.User{
		Model:             nil,
		CreateUserRequest: req,
	}
	// 密码加密
	if err := ins.BuildPassword(); err != nil {
		return nil, common.NewException(http.StatusBadRequest, err.Error())
	}
	// 创建用户
	if err := i.db.Model(&user.User{}).WithContext(ctx).Create(&ins).Error; err != nil {
		return nil, common.NewException(http.StatusBadRequest, err.Error())
	}
	return &ins, nil
}

func (i *Impl) DescribeUser(ctx context.Context, req *user.DescribeUserRequest) (*user.User, error) {
	// 数据校验
	if err := validator.New().Struct(req); err != nil {
		return nil, common.NewException(http.StatusBadRequest, err.Error())
	}
	// 构造容器
	ins := user.User{}
	// 构造SQL
	sql := i.db.Model(&user.User{}).WithContext(ctx)
	switch req.Type {
	case user.DescribeById:
		sql = sql.Where("id = ?", req.Value).First(&ins)
	case user.DescribeByName:
		sql = sql.Where("username = ?", req.Value).First(&ins)
	}
	// 返回结果
	if err := sql.Error; err != nil {
		return nil, common.NewException(http.StatusBadRequest, err.Error())
	}
	return &ins, nil
}

func (i *Impl) Login(ctx context.Context, req *user.LoginRequest) (*user.Token, error) {
	if err := validator.New().Struct(req); err != nil {
		return nil, common.NewException(http.StatusBadRequest, err.Error())
	}
	// 构造容器
	ins := user.User{}

	// 构造SQL
	if err := i.db.Model(&user.User{}).WithContext(ctx).Where("username = ?", req.Username).First(&ins).Error; err != nil {
		return nil, common.NewException(http.StatusBadRequest, err.Error())
	}
	// 密码校验
	if err := ins.CheckPassword(req.Password); err != nil {
		return nil, common.NewException(http.StatusBadRequest, err.Error())
	}

	// 生成Token
	token := user.NewToken(req.Username)
	exp := clause.OnConflict{
		Columns:   []clause.Column{{Name: "username"}},
		UpdateAll: true,
	}

	if err := i.db.Model(&user.Token{}).WithContext(ctx).Clauses(exp).Create(&token).Error; err != nil {
		return nil, common.NewException(http.StatusBadRequest, err.Error())
	}

	// 返回
	return token, nil
}

func (i *Impl) ValidateToken(ctx context.Context, req *user.ValidateTokenRequest) (*user.Token, error) {
	if err := validator.New().Struct(req); err != nil {
		return nil, common.NewException(http.StatusBadRequest, err.Error())
	}
	// token 查询
	ins := user.Token{}
	if err := i.db.Model(&user.Token{}).WithContext(ctx).Where(req).First(&ins).Error; err != nil {
		return nil, common.NewException(http.StatusBadRequest, err.Error())
	}

	// 过期判断
	if ins.IsExpire() {
		return nil, common.NewException(http.StatusBadRequest, "token 过期，请重新登录")
	}

	// 未过期，则延长过期时间
	ins.UpdatedAt = time.Now()
	i.db.Save(&ins)

	return &ins, nil
}
