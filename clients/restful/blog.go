package restful

import (
	"context"
	"encoding/json"
	"fmt"
	"gitee.com/qiaogy91_admin/vblog/apps/blog"
	"io"
	"net/http"
	"net/url"
)

type BlogClient struct {
	*Client
}

func (b *BlogClient) CreateBlog(ctx context.Context, ins *blog.CreateBlogRequest) (*blog.Blog, error) {
	//TODO implement me
	panic("implement me")
}

func (b *BlogClient) DeleteBlog(ctx context.Context, ins *blog.DeleteBlogRequest) (*blog.Blog, error) {
	//TODO implement me
	panic("implement me")
}

func (b *BlogClient) UpdateBlog(ctx context.Context, ins *blog.UpdateBlogRequest) (*blog.Blog, error) {
	//TODO implement me
	panic("implement me")
}

func (b *BlogClient) QueryBlog(ctx context.Context, ins *blog.QueryBlogRequest) (*blog.BlogsSet, error) {
	query := url.Values{}
	query.Add("page_num", fmt.Sprintf("%d", ins.PageNum))
	query.Add("page_size", fmt.Sprintf("%d", ins.PageSize))
	query.Add("key_word", fmt.Sprintf("%s", ins.KeyWord))
	query.Add("author", fmt.Sprintf("%s", ins.Author))

	req, err := http.NewRequest(http.MethodGet, "http://127.0.0.1:8080/vblog/api/v1/blog", nil)
	req.URL.RawQuery = query.Encode()

	if err != nil {
		return nil, err
	}

	rsp, err := b.Client.httpClient.Do(req)
	if err != nil {
		return nil, err
	}

	defer func() { _ = rsp.Body.Close() }()
	content, err := io.ReadAll(rsp.Body)
	if err != nil {
		return nil, err
	}

	blogSet := &blog.BlogsSet{}
	if err := json.Unmarshal(content, blogSet); err != nil {
		return nil, err
	}

	return blogSet, nil
}

func (b *BlogClient) DescribeBlog(ctx context.Context, ins *blog.DescribeBlogRequest) (*blog.Blog, error) {
	//TODO implement me
	panic("implement me")
}

var _ blog.Service = &BlogClient{}
