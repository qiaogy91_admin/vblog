package restful_test

import (
	"context"
	"gitee.com/qiaogy91_admin/vblog/apps/blog"
	"gitee.com/qiaogy91_admin/vblog/clients/restful"
	"testing"
)

var (
	ctx = context.Background()
)

func TestBlogClient_QueryBlog(t *testing.T) {
	c := restful.NewClient("127.0.0.1:8080")
	req := &blog.QueryBlogRequest{
		PageNum:  1,
		PageSize: 20,
		KeyWord:  "啊啊啊",
		Author:   "",
	}

	rsp, err := c.Blog.QueryBlog(ctx, req)
	if err != nil {
		t.Fatal(err)
	}

	t.Logf("%+v", rsp)
}
