package restful

import (
	"net/http"
	"net/http/cookiejar"
	"time"
)

func NewClient(addr string) (c *Client) {
	jar, err := cookiejar.New(nil)
	if err != nil {
		panic("生成jar err")
	}
	c = &Client{
		addr:       addr,
		token:      "",
		httpClient: http.Client{Jar: jar, Timeout: 10 * time.Second},
	}
	c.Blog = &BlogClient{c}
	c.User = &UserClient{c}
	return
}

// Client 是所有Impl 的基类，内部封装了HTTP Client
type Client struct {
	addr       string
	token      string
	httpClient http.Client

	Blog *BlogClient
	User *UserClient
}
