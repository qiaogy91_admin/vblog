package restful

import (
	"context"
	"gitee.com/qiaogy91_admin/vblog/apps/user"
)

type UserClient struct {
	*Client
}

func (u UserClient) CreateUser(ctx context.Context, req *user.CreateUserRequest) (*user.User, error) {
	//TODO implement me
	panic("implement me")
}

func (u UserClient) DescribeUser(ctx context.Context, req *user.DescribeUserRequest) (*user.User, error) {
	//TODO implement me
	panic("implement me")
}

func (u UserClient) Login(ctx context.Context, req *user.LoginRequest) (*user.Token, error) {
	//TODO implement me
	panic("implement me")
}

func (u UserClient) ValidateToken(ctx context.Context, req *user.ValidateTokenRequest) (*user.Token, error) {
	//TODO implement me
	panic("implement me")
}

var _ user.Service = &UserClient{}
