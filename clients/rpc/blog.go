package rpc

import (
	"context"
	"gitee.com/qiaogy91_admin/vblog/apps/blog"
)

type BlogClient struct {
	*Client
}

// RPC 注册要求服务端方法必须长得样子，没必要去定义服务端的实现
// 因此这里仅约束下客户端
var _ blog.Service = &BlogClient{}

func (b *BlogClient) CreateBlog(ctx context.Context, ins *blog.CreateBlogRequest) (*blog.Blog, error) {
	//TODO implement me
	panic("implement me")
}

func (b *BlogClient) DeleteBlog(ctx context.Context, ins *blog.DeleteBlogRequest) (*blog.Blog, error) {
	//TODO implement me
	panic("implement me")
}

func (b *BlogClient) UpdateBlog(ctx context.Context, ins *blog.UpdateBlogRequest) (*blog.Blog, error) {
	//TODO implement me
	panic("implement me")
}

func (b *BlogClient) QueryBlog(ctx context.Context, ins *blog.QueryBlogRequest) (*blog.BlogsSet, error) {
	//TODO implement me
	rsp := &blog.BlogsSet{}
	if err := b.client.Call("", ins, rsp); err != nil {
		return nil, err
	}
	return rsp, nil
}

func (b *BlogClient) DescribeBlog(ctx context.Context, ins *blog.DescribeBlogRequest) (*blog.Blog, error) {
	//TODO implement me
	panic("implement me")
}
