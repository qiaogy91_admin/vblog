package rpc

/*有时候直接用GRPC 太重了，内部小项目如果全部是Golang 写的，可以直接用 net/rpc 来实现rpc*/
import "net/rpc"

type Client struct {
	addr   string
	client *rpc.Client

	BlogClient *BlogClient
	UserClient *UserClient
}

func NewClient(addr string) (c *Client) {
	client, err := rpc.Dial("tcp", addr)
	if err != nil {
		panic(err)
	}

	c = &Client{addr: addr, client: client}
	c.BlogClient = &BlogClient{c}
	c.UserClient = &UserClient{c}
	return
}
