package grpc

import (
	"fmt"
	"gitee.com/qiaogy91_admin/vblog/apps/comment"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

func NewClient(host string, port int) *Client {
	conn, err := grpc.Dial(fmt.Sprintf("%s:%d", host, port), grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		panic(err)
	}

	return &Client{
		conn:    conn,
		Comment: comment.NewRpcClient(conn),
	}
}

type Client struct {
	conn    *grpc.ClientConn
	Comment comment.RpcClient
}

func (c *Client) Close() {
	if err := c.conn.Close(); err != nil {
		fmt.Printf("close conn failed: %v\n", err)
	}
}
