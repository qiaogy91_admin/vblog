package grpc_test

import (
	"context"
	"gitee.com/qiaogy91_admin/vblog/apps/comment"
	"gitee.com/qiaogy91_admin/vblog/clients/grpc"
	"testing"
)

func TestNewClient(t *testing.T) {
	client := grpc.NewClient("127.0.0.1", 18080)
	rsp, err := client.Comment.CreateComment(context.Background(), &comment.CreateCommentRequest{})
	if err != nil {
		t.Fatalf("create comment failed: %v\n", err)
	}
	t.Logf("rsp: %v\n", rsp)
}
